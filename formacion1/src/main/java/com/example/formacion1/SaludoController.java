package com.example.formacion1;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class SaludoController {
    @RequestMapping("/")
    public String index() {
        return "Hola, yo soy tu API";
    }
}
